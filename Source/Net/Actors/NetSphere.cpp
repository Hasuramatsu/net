// Fill out your copyright notice in the Description page of Project Settings.


#include "NetSphere.h"

#include "NetCharacter.h"

// Sets default values
ANetSphere::ANetSphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMeshComponent;

	bReplicates = true;
}

// Called when the game starts or when spawned
void ANetSphere::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANetSphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANetSphere::Grab_Implementation(AActor* Grabber)
{
	if(bCanBeGrabbed)
	{
		if(CurrentHolder)
		{
			CurrentHolder->SetHoldSphere(false);
		}
		const FAttachmentTransformRules Rule{EAttachmentRule::KeepWorld, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false};
		AttachToActor(Grabber, Rule);
		CurrentHolder = Cast<ANetCharacter>(Grabber);
		CurrentHolder->SetHoldSphere(true);
		SetActorRelativeLocation(FVector(150.f, 0.f, 0.f));
		bCanBeGrabbed = false;
		GetWorld()->GetTimerManager().SetTimer(GrabCooldownTimer, this, &ANetSphere::ResetGrab, GrabCooldown);
	}
}

void ANetSphere::ResetGrab()
{
	bCanBeGrabbed = true;
}