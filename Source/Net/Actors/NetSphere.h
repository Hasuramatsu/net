// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "NetSphere.generated.h"

class ANetCharacter;

UCLASS()
class NET_API ANetSphere : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANetSphere();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Grab
	UFUNCTION(NetMulticast, Reliable)
	void Grab(AActor* Grabber);
	//Getters
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool CheckCanBeGrabbed() const {return bCanBeGrabbed;}
	UFUNCTION(BlueprintPure)
	FORCEINLINE ANetCharacter* GetCurrentHolder() const {return CurrentHolder;}
protected:
	//Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* StaticMeshComponent;

	//Parameters
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grab")
	float GrabCooldown = 0.5f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	//Flags
	bool bCanBeGrabbed = true;

	//Parameters
	UPROPERTY()
	ANetCharacter* CurrentHolder = nullptr;

	//Timers
	FTimerHandle GrabCooldownTimer;

	//Func
	void ResetGrab();
};
