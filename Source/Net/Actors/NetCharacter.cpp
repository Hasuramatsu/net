// Fill out your copyright notice in the Description page of Project Settings.


#include "NetCharacter.h"


#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/WidgetComponent.h"



// Sets default values
ANetCharacter::ANetCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->bUsePawnControlRotation = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);
	CameraComponent->bUsePawnControlRotation = false;
	
	HoldWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HoldWidget"));
	HoldWidget->SetupAttachment(RootComponent);
	HoldWidget->SetHiddenInGame(true);
	
	GetCharacterMovement()->bOrientRotationToMovement = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

}

void ANetCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ANetCharacter, bHoldSphere);
	DOREPLIFETIME(ANetCharacter, CurrentNum);
}

// Called when the game starts or when spawned
void ANetCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ANetCharacter::Grab()
{
	if(!bHoldSphere)
	{
		FVector EndLocation = GetActorLocation() + (GetActorForwardVector() * 300.f);
		FHitResult HitResult;
		UKismetSystemLibrary::SphereTraceSingle(GetWorld() ,GetActorLocation(), EndLocation, 32.f, ETraceTypeQuery::TraceTypeQuery1, false, TArray<AActor*>{this}, EDrawDebugTrace::ForDuration, HitResult, true);
		if(HitResult.bBlockingHit)
		{
			ANetSphere* Sphere = Cast<ANetSphere>(HitResult.GetActor());
			if(Sphere && Sphere->CheckCanBeGrabbed())
			{
				HandleGrab(Sphere);
			}
		}
	}
}

void ANetCharacter::OnRep_CurrentNum()
{
	Material = GetMesh()->CreateDynamicMaterialInstance(0);
	if(CurrentNum > 0 && SkinColors.Num() >= CurrentNum && Material)
	{
		Material->SetVectorParameterValue("Color", SkinColors[CurrentNum - 1]);
	}
	else
	{
		if(SkinColors.Num() < CurrentNum)
		{
			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Red, TEXT("ANetCharacter::OnRep_CurrentNum -- No matching color"));
		}
		else if(!Material)
		{
			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Red, TEXT("ANetCharacter::OnRep_CurrentNum -- Material nullptr"));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Red, TEXT("ANetCharacter::OnRep_CurrentNum -- Unknown error"));
		}
	}


}

void ANetCharacter::SetHoldSphere_Implementation(bool Flag)
{
	bHoldSphere = Flag;
	if(GetLocalRole() == ROLE_Authority)
	{
		OnRep_HoldSphere();
	}
}

void ANetCharacter::SetColorOfMesh_Implementation(FLinearColor NewColor)
{
	Color = NewColor;
	if(Material)
	{
		Material->SetVectorParameterValue(TEXT("Color"), Color);
	}
}

void ANetCharacter::OnRep_HoldSphere()
{
	HoldWidget->SetHiddenInGame(!bHoldSphere);
	FString String = bHoldSphere? TEXT("True"):TEXT("False");
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, String);
}

void ANetCharacter::MoveForward(const float Value)
{
	if(Controller && Value != 0.0f)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(ForwardDirection, Value);
	}
}

void ANetCharacter::MoveRight(const float Value)
{
	if(Controller && Value != 0.0f)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(ForwardDirection, Value);
	}
}

void ANetCharacter::HandleGrab_Implementation(ANetSphere* Sphere)
{
	//if(GetLocalRole() == ROLE_Authority)
	//{
		//SphereRef = Sphere;
		//OnRep_SphereRef();
		Sphere->Grab(this);		
	//}
}



// Called every frame
void ANetCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ANetCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &ANetCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("Right"), this, &ANetCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Grab"), IE_Pressed, this, &ANetCharacter::Grab);
}

// void ANetCharacter::OnRep_SphereRef()
// {
// 	FAttachmentTransformRules Rule{EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, EAttachmentRule::SnapToTarget, false};
// 	if(SphereRef)
// 	{
// 		SphereRef->AttachToActor(this, Rule);
// 	}
// }
