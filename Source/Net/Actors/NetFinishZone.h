// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "NetFinishZone.generated.h"

UCLASS()
class NET_API ANetFinishZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANetFinishZone();

	// Called every frame
	virtual void Tick(float DeltaTime) override;



protected:
	//Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UBoxComponent* CollisionZone = nullptr;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	//Func
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UFUNCTION(Server, Reliable)
	void Overlap(class ANetSphere* Sphere);
};
