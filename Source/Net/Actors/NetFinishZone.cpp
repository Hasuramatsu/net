// Fill out your copyright notice in the Description page of Project Settings.


#include "NetFinishZone.h"


#include "NetCharacter.h"
#include "NetSphere.h"
#include "Kismet/GameplayStatics.h"
#include "Net/NetGameModeBase.h"

// Sets default values
ANetFinishZone::ANetFinishZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CollisionZone = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionZone"));
	RootComponent = CollisionZone;
}

// Called when the game starts or when spawned
void ANetFinishZone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANetFinishZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANetFinishZone::NotifyActorBeginOverlap(AActor* OtherActor)
{
	ANetSphere* Sphere = Cast<ANetSphere>(OtherActor);
	if(Sphere)
	{
		Overlap(Sphere);
	}
	
}

void ANetFinishZone::Overlap_Implementation(ANetSphere* Sphere)
{
	if(GetLocalRole() == ROLE_Authority)
	{
		ANetGameModeBase* MyGameMode = Cast<ANetGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if(MyGameMode)
		{
			MyGameMode->FinishGame(Cast<APlayerController>(Sphere->GetCurrentHolder()->GetController()));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("ANetFinishZone::Overlap_Implementation -- Failed cast Gamemode"));
		}
	}
}
