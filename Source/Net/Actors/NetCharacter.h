// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "NetSphere.h"

#include "GameFramework/Character.h"


#include "NetCharacter.generated.h"

UCLASS()
class NET_API ANetCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	//Parameters

	//Color
	UPROPERTY(ReplicatedUsing=OnRep_CurrentNum)
	int32 CurrentNum = 0;

	//Func

	//Hold
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void SetHoldSphere(const bool Flag);

	//MaterialColor
	UFUNCTION(NetMulticast, Unreliable)
	void SetColorOfMesh(FLinearColor NewColor);

	//RepNotify
	UFUNCTION()
	void OnRep_CurrentNum();
	UFUNCTION()
	void OnRep_HoldSphere();

	
	// Sets default values for this character's properties
	ANetCharacter();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
protected:
	//Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class USpringArmComponent* SpringArmComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UCameraComponent* CameraComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UWidgetComponent* HoldWidget;

	//Color
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MeshColor")
	TArray<FLinearColor> SkinColors;

	//Func
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Grab
	UFUNCTION(BlueprintCallable)
	void Grab();
	UFUNCTION(Server, Reliable)
	void HandleGrab(ANetSphere* Sphere);

private:	
	//Axis vars
	float AxisX = 0.f;
	float AxisY = 0.f;

	//Flags
	UPROPERTY(ReplicatedUsing=OnRep_HoldSphere)
	bool bHoldSphere = false;

	//Replication function

	
	//Input func
	void MoveForward(const float Value);
	void MoveRight(const float Value);

	//DynamicMateria
	UPROPERTY()
	UMaterialInstanceDynamic* Material = nullptr;
	FLinearColor Color;

};

