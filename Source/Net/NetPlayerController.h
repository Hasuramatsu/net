// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "NetPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class NET_API ANetPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	//Func
	UFUNCTION(Client, Reliable)
	void EndGame(bool Flag);
protected:
	//Func
	virtual void BeginPlay() override;
	
private:

};
