// Fill out your copyright notice in the Description page of Project Settings.


#include "NetPlayerController.h"

void ANetPlayerController::EndGame_Implementation(bool Flag)
{
	FString String = Flag ? TEXT("true") : TEXT("false");
	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Blue, String);
}

void ANetPlayerController::BeginPlay()
{
	Super::BeginPlay();
}
