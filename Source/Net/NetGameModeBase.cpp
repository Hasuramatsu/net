// Copyright Epic Games, Inc. All Rights Reserved.


#include "NetGameModeBase.h"


#include "NetPlayerController.h"
#include "Actors/NetCharacter.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"

void ANetGameModeBase::FinishGame(APlayerController* Winner)
{
	for(auto* It : GameState->PlayerArray)
	{
		ANetPlayerController* MyController = Cast<ANetPlayerController>(It->GetNetOwningPlayer()->PlayerController);
		if(MyController && MyController == Winner)
		{
			MyController->EndGame(true);
		}
		else if(MyController)
		{
			MyController->EndGame(false);
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("ANetGameModeBase::FinishGame -- Failed to cast to controller"));
		}
	}
}

void ANetGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	FString String = FString::Printf(TEXT("Player %d"), GetNumPlayers());
	NewPlayer->SetName(String);
	ANetCharacter* MyCharacter = Cast<ANetCharacter>(NewPlayer->GetPawn());
	if(MyCharacter && GetNumPlayers() > 0)
	{
		MyCharacter->CurrentNum = GetNumPlayers();
		if(MyCharacter->GetLocalRole() == ROLE_Authority)
		{
			MyCharacter->OnRep_CurrentNum();
		}
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, String);
	}
	else 
	{
		if(!MyCharacter)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("ANetGameModeBase::PostLogin -- failed to cast to character"));
		}
		if(GetNumPlayers() < 0)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("ANetGameModeBase::PostLogin -- Players < 0"));
		}
	}
}